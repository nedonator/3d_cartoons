#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 globalLight;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexUV;

out vec3 color;
out vec2 UV;
out vec4 position;
out vec4 normal;



void main()
{
    color = vec3(1, 1, 1);
    
    position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    normal = viewMatrix * vec4(vertexNormal, 0.0);
    float k = 0.7;
    float reflection = dot(normal, normalize(position)) * k + 1 - k;
    //float reflection = 1;
    gl_Position = position;
    
    float factor = 1 / (1 + dot(gl_Position, gl_Position)) + 0.1;
    color = color * factor;// * reflection;
    UV = vertexUV;
}
#version 330

in vec3 color;
in vec2 UV;
in vec4 position;
in vec4 normal;

out vec4 fragColor;

uniform sampler2D myTextureSampler;
uniform sampler2D mapping;

void main()
{
    vec3 textureColor = texture( myTextureSampler, UV ).rgb;
    fragColor = vec4(color, 0.0);
    float light = dot(normalize(position), vec4(0, 0, 1, 0));
    float k = 0.5;
    vec4 mapped_normal = normal * vec4(texture(mapping, UV).rgb, 0.0);
    float reflection = dot(mapped_normal, normalize(position)) * k + 1 - k;
    light = light * 30 - 15;
    fragColor = fragColor * vec4(textureColor, 0.0) * reflection * light; 
}
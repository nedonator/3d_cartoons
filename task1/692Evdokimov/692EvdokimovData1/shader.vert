#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 globalLight;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    float factor = 1;
    color = vec4(vertexNormal, 0.0) * 0.5 + 0.5;
    //color = viewMatrix * vec4(vertexNormal, 0.0) * 0.5 + 0.5;
    vec4 red = vec4(1, 1, 0, 0);
    vec4 dir = viewMatrix * vec4(vertexNormal, 0.0);
    float scalar = length(cross(vec3(red), vec3(dir))) / 4;
    //color = modelMatrix * vec4(1.0, 0.0, 0.0, 0.0) * 0.5 + 0.5;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    factor = 1 / exp(1.5f * length(gl_Position));
    float light = dot(globalLight, vertexNormal) + 0.1;
    color = vec4(light, light / 2, scalar, 0);
    color = color * factor + (1 - factor) / 4;
    
}
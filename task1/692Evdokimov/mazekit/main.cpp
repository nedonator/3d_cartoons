#include "maze_application.hpp"
#include <ctime>


int main(int argc, char** argv)
{
    //std::srand(std::time(nullptr));
    std::srand(42);
    mazekit::MazeApplication mazeApplication;
    mazeApplication.start();
    return 0;
}
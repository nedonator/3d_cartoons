#include "maze.hpp"
#include "maze_camera_mover.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>
#include <cmath>


namespace mazekit {

namespace {

static const float MARGIN_WIDTH = 0.4f;

}

MazeCameraMover::MazeCameraMover(mazekit::MazePtr maze, int a, int b) :
	CameraMover(),
	oldXPosition_(0),
	oldYPosition_(0),
	cameraPosition_(-1.f + a / 2.f, -1.f + b / 2.f, 0.5f)
{
	cameraRotation_ = glm::vec3(-glm::pi<float>()/2.f, -glm::pi<float>(), 0);
	maze_ = maze;
}

void MazeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void MazeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	const int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (state == GLFW_PRESS)
	{
		const double dx = xpos - oldXPosition_;
		const double dy = ypos - oldYPosition_;

		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * cameraRotation_;
		cameraRotation_ *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

		glm::vec3 upDir(0.0f, 0.0f, 1.0f);
		cameraRotation_ *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
	}

	oldXPosition_ = xpos;
	oldYPosition_ = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCameraMover::update(GLFWwindow* window, double dt)
{
	const float speed = 1.0f;
	glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * cameraRotation_;
	forwDir.z = 0;
	forwDir = glm::normalize(forwDir);

	const glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * cameraRotation_;

	const float _prevposZ = cameraPosition_.z;
	const float _prevPosX = cameraPosition_.x;
	const float _prevPosY = cameraPosition_.y;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		cameraPosition_ += forwDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		cameraPosition_ -= forwDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		cameraPosition_ -= rightDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		cameraPosition_ += rightDir * speed * static_cast<float>(dt);
	}

	cameraPosition_.z = _prevposZ;

	if (checkCollision(glm::vec2(cameraPosition_.x, cameraPosition_.y))) {
		cameraPosition_.x = _prevPosX;
		cameraPosition_.y = _prevPosY;
	}

	_camera.viewMatrix = glm::toMat4(-cameraRotation_) * glm::translate(-cameraPosition_);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

bool MazeCameraMover::checkCollision(glm::vec2 position) {
	float x = position.x + maze_->width()/2.f + 0.25f;
	float y = position.y + maze_->height()/2.f + 0.25f;
	int i = (int)x;
	int j = (int)y;
	if (i >= 0 && i < maze_->width() && j >= 0 && j < maze_->height()) {
		vertex v = maze_->getVertex(i, j);
		if (x - i >= 0.4f) {
			if (y - j >= 0.4f) {
				return true;
			}
			if (y - j <= 0.1f)
				return true;
			else return v.hasWall[3];
		}
		else if (x - i <= 0.1f) {
			if (y - j >= 0.4f) {
				return true;
			}
			if (y - j <= 0.1f)
				return true;
			else return v.hasWall[1];
		}
		else if (y - j >= 0.4f) {
			return v.hasWall[2];
		}
		else if (y - j <= 0.1f)
			return v.hasWall[0];
	}
	return false;
}

}
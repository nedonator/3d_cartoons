#pragma once


#include <fstream>
#include <memory>
#include <vector>
#include "../common/Mesh.hpp"
#include "vertex.hpp"
#include "renderModes.hpp"
#include "Texture.hpp"

namespace mazekit {

	class Maze;

	typedef std::shared_ptr<Maze> MazePtr;

	class Maze {
	public:
		Maze(unsigned x, unsigned y);
		~Maze() {};

		int width() const { return width_; };
		int height() const { return height_; };
		void init();

	private:
		std::vector<std::vector<vertex*> > vertexes;
		TexturePtr cat_texture;
		TexturePtr grass_texture;
		TexturePtr stupid_mapping;
		int width_;
		int height_;

		void draw_vertex(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, vertex v);
		void draw_x(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, int i, int j);
		void draw_y(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, int i, int j);

	public:
		vertex getVertex(int i, int j) {
			return *vertexes[i][j];
		}

		glm::vec3 light(float t) {
			return glm::vec3(0.55 * cos(t), 0, 0.55 * sin(t));
		}

		void draw(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, double x, double y);

	};

}
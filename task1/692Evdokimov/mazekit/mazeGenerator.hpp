#include <vector>
#include "vertex.hpp"

namespace mazekit {

	class mazeGenerator {
    public:

        std::vector<std::vector<vertex*>> makeMaze(int x, int y) {
            std::vector<std::vector<vertex*>>v = *(new std::vector<std::vector<vertex*>>());
            std::vector<std::vector<bool>> visited;
            v.resize(x);
            visited.resize(x);
            for (int i = 0; i < x; i++) {
                v[i].resize(y);
                visited[i].resize(y);
                for (int j = 0; j < y; j++) {
                    v[i][j] = new vertex();
                    v[i][j]->setPlace(i - x/2.f, j - y/2.f);
                }
            }
            std::vector<int*> toVisit;
            std::vector<int> a;
            toVisit.push_back(new int[3]{ 3, 1, 0 });
            int* next;
            int nextNum;
            while ((nextNum = choose(toVisit.size())) != -1) {
                next = toVisit[nextNum];
                toVisit[nextNum] = toVisit[toVisit.size() - 1];
                toVisit.pop_back();
                int i = next[1];
                int j = next[2];
                if (!visited[i][j]) {
                    visited[i][j] = true;
                    if (i > 0)toVisit.push_back(new int[3]{ 1, i - 1, j });
                    if (j > 0)toVisit.push_back(new int[3]{ 0, i, j - 1 });
                    if (i < x - 1)toVisit.push_back(new int[3]{ 3, i + 1, j });
                    if (j < y - 1)toVisit.push_back(new int[3]{ 2, i, j + 1 });
                } else if (rand() % (x + y)) continue;
                switch (next[0]) {
                case 0: v[i][j + 1]->hasWall[0] = false, v[i][j]->hasWall[2] = false; break;
                case 1: v[i + 1][j]->hasWall[1] = false, v[i][j]->hasWall[3] = false; break;
                case 2: v[i][j - 1]->hasWall[2] = false, v[i][j]->hasWall[0] = false; break;
                case 3: v[i - 1][j]->hasWall[3] = false, v[i][j]->hasWall[1] = false; break;
                }
            }
            v[0][0]->hasWall[0] = false;
            return v;
		}

    private:
        static int choose(int n) {
            if (n == 0) return -1;
            while (n-- > 1)
                if (rand() % 2) break;
            return n;
        }
	};
}
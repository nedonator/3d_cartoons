#include "maze_application.hpp"

#include "meshes/parallelepiped.hpp"

#include <iostream>

namespace mazekit {

namespace {

static const std::string SHADER_VERT_FILE_PATH = "692EvdokimovData1/lamp.vert";
static const std::string SHADER_FRAG_FILE_PATH = "692EvdokimovData1/shader.frag";
static const int a_ = 20;
static const int b_ = 20;

static const float MARGIN_WIDTH = 0.1f;
static std::shared_ptr<MazeCameraMover> camera_;

}

MazeApplication::MazeApplication() : 
	Application()
{
	_cameraMover = std::make_shared<OrbitCameraMover>();
	activeCamera_ = _cameraMover;

	maze_ = std::make_shared<mazekit::Maze>(a_, b_);

	camera_ = std::make_shared<MazeCameraMover>(maze_, a_, b_);
	inactiveCamera_ = camera_;


	
}

void MazeApplication::makeScene()
{
	Application::makeScene();

	shader_ = std::make_shared<ShaderProgram>(
		SHADER_VERT_FILE_PATH,
		SHADER_FRAG_FILE_PATH
	);

	xSize_ = maze_->width();
	ySize_ = maze_->height();

	init();
	floorMesh_ = mazekit::meshes::makeParallelepiped(a_ + .5f, b_ + .5f, .05f);
	floorMesh_->setModelMatrix(
		glm::translate(
			glm::mat4(1.0f),
			glm::vec3(
				-0.5f,
				-0.5f,
				0.25f
			)
		)
	);
	ceilMesh_ = mazekit::meshes::makeParallelepiped(a_ + .5f, b_ + .5f, .05f);
	ceilMesh_->setModelMatrix(
		glm::translate(
			glm::mat4(1.0f),
			glm::vec3(
				-0.5f,
				-0.5f,
				0.75f
			)
		)
	);
}

void MazeApplication::draw()
{
	Application::draw();
	int width, height;

	glfwGetFramebufferSize(_window, &width, &height);
	glViewport(0, 0, width, height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader_->use();

	shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
	shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);


	maze_->draw(shader_, renderMode_, maze_->light(_oldTime), camera_->cameraPosition_[0] + a_/2. + .5, camera_->cameraPosition_[1] + b_/2. + .5);
}

void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_V) {
			activeCamera_.swap(inactiveCamera_);
			_cameraMover = activeCamera_;
		}
		else if (key == GLFW_KEY_M) {
			drawCeil_ = !drawCeil_;
		}
		else if (key == GLFW_KEY_N) {
			renderMode_ = (RenderMode)((renderMode_ + 1) % 2);
		}
		Application::handleKey(key, scancode, action, mods);
	}
}

void MazeApplication::init(){
	maze_->init();
}

}
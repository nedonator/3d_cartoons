#include "maze.hpp"
#include "vertex.hpp"
#include <vector>
#include "mazeGenerator.hpp"
#include <Texture.hpp>
#include <iostream>


namespace mazekit {

    Maze::Maze(unsigned x, unsigned y) {
        width_ = x;
        height_ = y;
        vertexes = (new mazeGenerator())->makeMaze(x, y);
    }
    void Maze::init() {
        for (std::vector<vertex*> line : vertexes)
            for (vertex* v : line) {
                v->build();
            }
        cat_texture = loadTexture("D:\\\\�����\\��������\\arina5arina-opengl_tasks_2020-a537b4755763\\arina5arina-opengl_tasks_2020-a537b4755763\\task1\\692Evdokimov\\692EvdokimovData1\\texture.bmp");
        grass_texture = loadTexture("D:\\\\�����\\��������\\arina5arina-opengl_tasks_2020-a537b4755763\\arina5arina-opengl_tasks_2020-a537b4755763\\task1\\692Evdokimov\\692EvdokimovData1\\grass.bmp");
        stupid_mapping = loadTexture("D:\\\\�����\\��������\\arina5arina-opengl_tasks_2020-a537b4755763\\arina5arina-opengl_tasks_2020-a537b4755763\\task1\\692Evdokimov\\692EvdokimovData1\\mapping.bmp");
        glActiveTexture(GL_TEXTURE1);
        stupid_mapping->bind();
        glActiveTexture(GL_TEXTURE0);
        //TexturePtr texture = loadTexture("D:\\\\�����");
        //if(texture != NULL)

    }

    void Maze::draw(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, double x, double y) {
        switch (mode) {
        case SIMPLE:
            for (auto vec : vertexes)
                for (vertex* v : vec)
                    draw_vertex(shader, mode, light, *v);
            break;
        case OPTIMIZED:
            int i = x;
            int j = y;
            vertex* v = NULL;
            try {
                v = vertexes.at(i).at(j);
            }
            catch (std::exception e) {};
            if (v != NULL) {
                draw_vertex(shader, mode, light, *v);
                draw_x(shader, mode, light, i, j);
                draw_y(shader, mode, light, i, j);
                if (!v->hasWall[0] && j > 0)
                    draw_x(shader, mode, light, i, j - 1);
                if (!v->hasWall[1] && i > 0)
                    draw_y(shader, mode, light, i - 1, j);
                if (!v->hasWall[2] && j < height_ - 1)
                    draw_x(shader, mode, light, i, j + 1);
                if (!v->hasWall[3] && i < width_ - 1)
                    draw_y(shader, mode, light, i + 1, j);
            }
        }
    }

    void Maze::draw_x(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, int i, int j) {
        int i1 = i;
        int j1 = j;
        while (!vertexes[i1][j1]->hasWall[1] && i1 > 0) {
            draw_vertex(shader, mode, light, *vertexes[--i1][j1]);
        }
        i1 = i;
        while (!vertexes[i1][j1]->hasWall[3] && i1 < width_ - 1) {
            draw_vertex(shader, mode, light, *vertexes[++i1][j1]);
        }
    }

    void Maze::draw_y(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, int i, int j) {
        int i1 = i;
        int j1 = j;
        while (!vertexes[i1][j1]->hasWall[0] && j1 > 0) {
            draw_vertex(shader, mode, light, *vertexes[i1][--j1]);
        }
        j1 = j;
        while (!vertexes[i1][j1]->hasWall[2] && j1 < height_ - 1) {
            draw_vertex(shader, mode, light, *vertexes[i1][++j1]);
        }
    }

    void Maze::draw_vertex(ShaderProgramPtr shader, RenderMode mode, glm::vec3 light, vertex v) {
        
        for (MeshPtr m : v.meshes) {
            if (m->modelMatrix()[3][2])
                cat_texture->bind();
            else
                grass_texture->bind();
            shader->setMat4Uniform("modelMatrix", m->modelMatrix());
            shader->setVec3Uniform("globalLight", light);
            m->draw();
        }
        if (mode == OPTIMIZED) {
            for (MeshPtr m : v.add_meshes) {
                if (m->modelMatrix()[3][2])
                    cat_texture->bind();
                else
                    grass_texture->bind();
                shader->setMat4Uniform("modelMatrix", m->modelMatrix());
                shader->setVec3Uniform("globalLight", light);
                m->draw();
            }
        }
    }
}
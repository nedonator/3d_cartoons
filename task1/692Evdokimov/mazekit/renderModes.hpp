#pragma once

namespace mazekit {

	enum RenderMode {
		SIMPLE,
		OPTIMIZED,
		TEXTURED
	};

}
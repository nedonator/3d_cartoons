#pragma once

#include "../common/Application.hpp"
#include "../common/Mesh.hpp"
#include "../common/ShaderProgram.hpp"
#include <meshes\parallelepiped.hpp>

namespace mazekit {

	class vertex {
		float x;
		float y;
		const float size_ = .5f;

	public:
		//0 - ����� y, 1 - ����� x, 2 - ���� y, 3 - ���� x
		bool hasWall[4];
		std::vector<MeshPtr> meshes;
		std::vector<MeshPtr> add_meshes;

		vertex() {
			x = 0;
			y = 0;
			hasWall[0] = true;
			hasWall[1] = true;
			hasWall[2] = true;
			hasWall[3] = true;
		}

		void setPlace(float x, float y) {
			this->x = x;
			this->y = y;
		}

		void build() {
			//if (x <= 0.1f) {
				add(x - size_, y - size_, x > 0.1f);
				add(x - size_, y, hasWall[1] ? 0 : -size_, x > 0.1f);
				//add(x - size_, y + size_, true);
			//}
			//if (y <= 0.1f) {
				add(x + size_, y - size_, y > 0.1f);
				add(x, y - size_, hasWall[0] ? 0 : -size_, y > 0.1f);
			//}
			add(x + size_, y + size_, false);
			add(x - size_, y + size_, true);

			
			add(x, y + size_, hasWall[2] ? 0 : -size_, false);
			add(x + size_, y, hasWall[3] ? 0 : -size_, false);
			/*if (hasWall[0])
				add(x, y - size_);
			else {
				add(x, y - size_)
			}
			if (hasWall[2])
				add(x, y + size_);
			if (hasWall[3])
				add(x + size_, y);*/
			add(x, y, -size_, false);
		}

	private:

		void add(float x, float y, float z, bool additional) {

			MeshPtr mesh = mazekit::meshes::makeParallelepiped(size_, size_, size_);
			mesh->setModelMatrix(
				glm::translate(
					glm::mat4(1.0f),
					glm::vec3(
						x,
						y,
						z + 0.5f
					)
				)
			);
			if (additional)
				add_meshes.push_back(mesh);
			else {
				meshes.push_back(mesh);
			}
		}

		void add(float x, float y, bool additional) {
			add(x, y, 0, additional);
		}
	};

}
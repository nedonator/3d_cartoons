#pragma once

#include "../common/Application.hpp"
#include "../common/Mesh.hpp"
#include "../common/ShaderProgram.hpp"

#include "maze.hpp"
#include "maze_camera_mover.hpp"

#include <iostream>


namespace mazekit {

class MazeApplication : public Application {
public:
	MazeApplication();

protected:
	void makeScene() override;
	void draw() override;
	void handleKey(int key, int scancode, int action, int mods) override;

private:
	void init();

	MeshPtr floorMesh_;
	MeshPtr ceilMesh_;

	ShaderProgramPtr shader_;

	mazekit::MazePtr maze_;

	CameraMoverPtr activeCamera_;
	CameraMoverPtr inactiveCamera_;

	RenderMode renderMode_ = SIMPLE;

	int xSize_;
	int ySize_;
	
	bool drawCeil_ = false;
};

}